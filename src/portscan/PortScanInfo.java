package portscan;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * 每进行一次端口扫描的时候，需要的基础信息
 * @author Administrator
 *
 */
public class PortScanInfo {
	String mIp;
	int mStartPort =0,mEndPort = 65525;
	
	public PortScanInfo(String ip,int startPort,int endPort){
		this.mIp = ip;
		this.mStartPort = startPort;
		this.mEndPort = endPort;
	}
	
	public String getIp() {
		return mIp;
	}

	public void setIp(String ip) {
		this.mIp = ip;
	}

	public int getStartPort() {
		return mStartPort;
	}

	public void setStartPort(int port) {
		this.mStartPort = port;
	}
	
	public int getEndPort() {
		return mEndPort;
	}

	public void setEndPort(int endPort) {
		this.mEndPort = endPort;
	}	
}
