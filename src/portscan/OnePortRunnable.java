package portscan;

import java.net.Socket;

/**
 * 这一个Runnable只能用来确定一个端口
 * 
 * @author wangfan
 *
 */
public class OnePortRunnable implements Runnable {

	private PortScanInfo mPortScanInfo = null;
	private IPortScanCallBack mPortScanCallBack = null;

	public OnePortRunnable(PortScanInfo portscanx,
			IPortScanCallBack portScanCallBack) {
		mPortScanInfo = portscanx;
		mPortScanCallBack = portScanCallBack;
	}

	@Override
	public void run() {
		while (mPortScanInfo.getStartPort() <= mPortScanInfo.getEndPort()) {
			try {
				Socket socket = new Socket(mPortScanInfo.getIp(),
						mPortScanInfo.getStartPort());

				if (socket.isConnected()) {
					// 成功扫描到一个端口
					mPortScanCallBack.onPortConnectSuccess(
							mPortScanInfo.getIp(),
							"" + mPortScanInfo.getStartPort());
				}
			}
			// catch (UnknownHostException e) {
			// e.printStackTrace();
			// } catch (IOException e) {
			//
			// }
			catch (Exception e) {
				// 扫描制定端口未开放
				mPortScanCallBack.onPortConnectFailed(mPortScanInfo.getIp(), ""
						+ mPortScanInfo.getStartPort());

				System.out.println("---error = " + e.toString());
			}

			mPortScanInfo.setStartPort(mPortScanInfo.getStartPort() + 1);
		}
	}

	/**
	 * 端口扫描的回调接口
	 * 
	 * @author wangfan
	 * @since 2015-6-28
	 *
	 */
	public interface IPortScanCallBack {
		/**
		 * 端口连接成功
		 */
		public void onPortConnectSuccess(String ip, String port);

		/**
		 * 端口连接失败
		 */
		public void onPortConnectFailed(String ip, String port);
	}

}
