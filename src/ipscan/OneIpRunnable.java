package ipscan;

import java.net.Socket;

public 	class OneIpRunnable implements Runnable{
    
	IPScanInfo mIpscanInfo;
	IOnIpScanCallBack mIOnIpScanCallBack;
	
	public OneIpRunnable(IPScanInfo ipScanInfo, IOnIpScanCallBack iOnIpScanCallBack){			
		mIpscanInfo = ipScanInfo;
		mIOnIpScanCallBack = iOnIpScanCallBack;
	}		
	
	@Override
	public void run() {
		int port;
		Socket socket = new Socket();
		for(int i = 0;i < mIpscanInfo.portArray.size();i ++){
			port = mIpscanInfo.portArray.get(i);
			
			try {
				socket.close();
				socket = new Socket(mIpscanInfo.getStartIP(),port);
				if(socket.isConnected()){
					//主机在线
					mIOnIpScanCallBack.onIpScanSucess(mIpscanInfo.getStartIP(), ""+port);
					System.out.println("ip scan success for " + mIpscanInfo.getStartIP());
					return;
				}
			
			} 
//			catch (UnknownHostException e) {
//				e.printStackTrace();
//			} catch (IOException e) {
//				
//			}
			catch(Exception e){
			}
		}
		
		//通过所有的预置端口检测发现主机不在线
		mIOnIpScanCallBack.onIpScanFailed(mIpscanInfo.getStartIP());	
	}

	/**
	 * IP地址扫描的回调接口
	 * @author wangfan
	 *
	 */
	public interface IOnIpScanCallBack{
		/**
		 * 具体的Ip正在被扫描
		 * @param ip
		 */
		public void onIpScanFailed(String ip);
		
		/**
		 * 具体的IP的具体端口正在被扫描
		 * @param ip
		 * @param port
		 */
		public void onIpScanSucess(String ip,String port);
	}
	
}
