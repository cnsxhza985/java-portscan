import ipscan.IPScanInfo;

/**
 * 一些辅助工具
 * @author Administrator
 *
 */
public class ResourceDispatcher {
 
	private static ResourceDispatcher sInstance;
	
	public static ResourceDispatcher getInstance(){
		if(sInstance == null){
			synchronized(ResourceDispatcher.class){
				if(sInstance == null){
					sInstance = new ResourceDispatcher();
				}
			}
		}
		
		return sInstance;
	} 
	
	/**
	 * 依照当前的IP，线性的递加启始IP地址
	 * @param mIPScanInfo
	 * @return
	 */
	public synchronized IPScanInfo getNextIPScanInfo(IPScanInfo mIPScanInfo){
		IPScanInfo ipscan = new IPScanInfo();
		
		String[] ipcodes = mIPScanInfo.startIP.split("[.]");
		int temp = Integer.parseInt(ipcodes[3]);
		temp ++;
		//startIP will plus 1 according to the param
		mIPScanInfo.startIP = ipcodes[0] + "." + ipcodes[1] + "." + ipcodes[2] + "." + temp;
		
		ipscan.setStartIP(mIPScanInfo.getStartIP());
		ipscan.setEndIP(mIPScanInfo.getEndIP());
		
		return ipscan;
	}	
	
}
