

import ipscan.IPScanInfo;
import ipscan.OneIpRunnable;
import ipscan.OneIpRunnable.IOnIpScanCallBack;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import portscan.PortScanInfo;
import portscan.OnePortRunnable;
import portscan.OnePortRunnable.IPortScanCallBack;

public class PortScanMainUI extends JFrame implements ActionListener, IOnIpScanCallBack, IPortScanCallBack{
	//卡片布局
	JTabbedPane jtp = new JTabbedPane();
	JPanel ipPanel = new JPanel();
	JPanel portPanel = new JPanel();
	JTextField ipField = new JTextField("192.168.1.100",20);
	JLabel label1 = new JLabel("请输入起始ip");
	JTextField ipField2= new JTextField("192.168.1.130",20);
	JLabel label2 = new JLabel("请输入终止ip");
	JButton startButton = new JButton("开始");
	JTextArea jta0 = new JTextArea("在该网段下使用的ip地址:\n");
	JTextArea jta1 = new JTextArea("在该网段下未使用的ip地址：\n");//ip扫描结果
	
	//端口扫描部分
	JLabel label3 = new JLabel("请输入要扫描的主机的ip地址：");
	JTextField ipFieldForPortScan = new JTextField("127.0.0.1",20);
	JButton startPortScan = new JButton("开始端口扫描");
	JTextArea jta2 = new JTextArea("目标主机开放的端口:\n");//显示端口扫描结果，此处显示开放的端口
	JTextArea jta3 = new JTextArea("目标主机未开放的端口:\n");
	JLabel label4 = new JLabel("请输入开始端口：");
	JLabel label5 = new JLabel("请输入终止端口：");
	JTextField mStartPort_ET = new JTextField("8000",10);
	JTextField mEndPort_ET = new JTextField("8080",10);
	IPScanInfo mIPScanInfo = null;//扫描ip地址是否在该网段下使用
	PortScanInfo mPortScanInfo = null;
	
	
	public PortScanMainUI(){
		init();
		ipScaninit();
		portScanInit();
	}
	
	/**
	 * 程序主界面设置
	 */
	private void init(){
		//ipPanel.setLayout(new GridLayout(0,2));
		
		this.add(jtp);
		this.setResizable(false);
		this.setVisible(true);
		this.setSize(800, 600);
		this.setTitle("端口扫描器");
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);  //屏蔽掉关闭按钮
		this.setBackground(Color.cyan);
		this.setLocationRelativeTo(null);
	}
	
	private void portScanInit(){	
		jtp.add(portPanel,"port扫描");
		JPanel portPanelNorth = new JPanel();
		JPanel portPanelCenter = new JPanel();
		JScrollPane jsp1 = new JScrollPane(jta2);
		JScrollPane jsp2 = new JScrollPane(jta3);
		
		portPanelCenter.setLayout(new GridLayout(0,2));
		
		portPanelNorth.setLayout(new GridLayout(0,2));
		portPanel.setLayout(new BorderLayout());
		portPanelNorth.add(label3);
		portPanelNorth.add(ipFieldForPortScan);
		portPanelNorth.add(label4);
		portPanelNorth.add(mStartPort_ET);
		portPanelNorth.add(label5);
		portPanelNorth.add(mEndPort_ET);		
		portPanelNorth.add(startPortScan);
		portPanel.add(portPanelNorth,BorderLayout.NORTH);
		portPanelCenter.add(jsp1);	
		portPanelCenter.add(jsp2);
		
		portPanel.add(portPanelCenter);
		startPortScan.addActionListener(this);
		mStartPort_ET.addActionListener(this);
	}
	
	private void ipScaninit(){
		jtp.add(ipPanel,"ip扫描");
		ipPanel.setLayout(new BorderLayout());
		
		JPanel panelTop = new JPanel();
		JPanel panelCenter = new JPanel(); 
		panelCenter.setLayout(new GridLayout(0,2));
		panelTop.add(label1);
		panelTop.add(ipField);
		panelTop.add(label2);
		panelTop.add(ipField2);
		panelTop.add(startButton);
		
		JScrollPane jsp1 = new JScrollPane(jta0);
		JScrollPane jsp2 = new JScrollPane(jta1);
		
		panelCenter.add(jsp1);
		panelCenter.add(jsp2);
		
		ipPanel.add(panelTop,BorderLayout.NORTH);
		ipPanel.add(panelCenter,BorderLayout.CENTER);
		
		startButton.addActionListener(this);
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == startButton){
			if(mIPScanInfo == null){
				//点击开始扫描ip地址
				int ipNum = 50;
				
				mIPScanInfo = new IPScanInfo();
				
				mIPScanInfo.setStartIP(ipField.getText());
				mIPScanInfo.setEndIP(ipField2.getText());
				
				ipNum = Utils.calculateIPCount(mIPScanInfo);
				if(ipNum > 0){
					ExecutorService executor = Executors.newFixedThreadPool(5);
					for(int i = 0;i < ipNum;i ++){
						executor.execute(new OneIpRunnable(ResourceDispatcher.getInstance().getNextIPScanInfo(mIPScanInfo),this));
					}	
				}else{
					System.out.println(">>>>sorry end ip should bigger than begin ip");
				}
			}else{
				System.out.println(">>>>sorry ipscan has runed");
			}
		}
		
		if(e.getSource() == this.startPortScan){
			if(mPortScanInfo == null){
				String ip = ipFieldForPortScan.getText().trim();
				int startPort = Integer.parseInt(mStartPort_ET.getText());
				int endPort = Integer.parseInt(mEndPort_ET.getText());
				if( (endPort >= 0) && (endPort >= startPort)){
					mPortScanInfo = new PortScanInfo(ip,startPort,endPort);
					new Thread(new OnePortRunnable(mPortScanInfo,this)).start();
				}else{
					System.out.println(">>>>sorry  (endPort >= 0) && (endPort >= startPort)");
				}

			}else{
				System.out.println(">>>>sorry port scan has runed");
			}
		}
		
		if(e.getSource() == this.mStartPort_ET){
			System.exit(0);
		}
	}
	
	@Override
	public void onIpScanFailed(String ip) {
		jta1.append(ip + "\n");
		System.out.println("--bad "+ip);
	}

	@Override
	public void onIpScanSucess(String ip, String port) {
		jta0.append(ip + "\n");
		System.out.println("--good "+ip);
	}

	@Override
	public void onPortConnectSuccess(String ip, String port) {
		jta2.append(port + "\n");		
	}

	@Override
	public void onPortConnectFailed(String ip, String port) {
		jta3.append(port + "\n");		
	}
	
}