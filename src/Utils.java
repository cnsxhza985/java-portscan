import ipscan.IPScanInfo;


public class Utils {
	/**
	 * 计算两个连续的Ip地址之间的Ip地址数量
	 * @param iPScanInfo
	 * @return
	 */
	public static int calculateIPCount(IPScanInfo iPScanInfo){
		String[] ipCodes = iPScanInfo.getStartIP().split("[.]");
		int temp = Integer.parseInt(ipCodes[3]);
		String[] ipCodes1 = iPScanInfo.getEndIP().split("[.]");
		int temp1 = Integer.parseInt(ipCodes1[3]);
		return temp1 - temp;		
	}
}
